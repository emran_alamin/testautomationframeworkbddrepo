#Feature is like user story
Feature: Walmart Product price validations
  

  #scenario is called test case
  Scenario: Verify walmart product price with db
    Given I am in walmart landing page 
    When I enter product id
    Then get the product price
    Then get the product price from db
    And verify UI price is equal to db price
    
# given when are Test Steps

