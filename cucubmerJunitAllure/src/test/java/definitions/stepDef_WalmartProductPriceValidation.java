package definitions;

import static org.junit.Assert.assertEquals;


import java.sql.ResultSet;
import java.sql.SQLException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import base_allure.Base_Allure;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class stepDef_WalmartProductPriceValidation extends Base_Allure {
	String prodPrice_DB;
	String actualPrice;
	@Given("I am in walmart landing page")
	public void i_am_in_walmart_landing_page() {
		navigateURL("https://www.staples.com");
	}

	@When("I enter product id")
	public void i_enter_product_id() {
	    //sendKeys(By.id("search-field"), int1 + Keys.ENTER);
	    sendKeys(By.id("searchInput"), "24424023" + Keys.ENTER);

	}

	@Then("get the product price")
	public void get_the_product_price() throws InterruptedException {
		Thread.sleep(3000);
		actualPrice= getText(By.xpath("//div[@class='price-info__final_price_sku']"));
		actualPrice=actualPrice.replace("$", "");
	}

	@Then("get the product price from db")
	public void get_the_product_price_from_db() throws SQLException {
		//System.out.println(stmt);
		ResultSet  rs = stmt.executeQuery("SELECT prodPrice FROM tblProduct WHERE prodId=24424023");
		while (rs.next()) {
			prodPrice_DB =  rs.getString("prodPrice") ;
		}
	}

	@Then("verify UI price is equal to db price")
	public void verify_UI_price_is_equal_to_db_price() {
		assertEquals(prodPrice_DB, actualPrice);
	
	}

}
