package definitions;


import java.sql.DriverManager;
import java.sql.SQLException;


import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import base_allure.Base_Allure;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Hooks extends Base_Allure{
	
	
	@Before
	public void setup() throws SQLException {
		ChromeOptions option = new ChromeOptions();
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver(option);
		
		//mySql db connection instantiation
		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/dbmicrotech", "root", "root" );
		
		//mySql Statement 
		stmt = conn.createStatement();

	}

	@After
	public void tearDown() throws SQLException {
		//driver.quit();
		//connection close
		if (!conn.isClosed()) {
			conn.close();
		}
	}

}
