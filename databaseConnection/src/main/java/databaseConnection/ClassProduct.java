package databaseConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ClassProduct {

	public static void main(String[] args) throws SQLException {
		double actualProdPrice=2.00;
		
//      https://downloads.mysql.com/archives/c-j
		
		//local connect db 
		Connection conn;
		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/dbmicrotech", "root", "root" );

		//declare output variable
		Statement stmt;
		stmt = conn.createStatement();

		//set output
		ResultSet  rs = stmt.executeQuery("select prodPrice from tblProduct WHERE prodName='Pen'");
		
		
		//print output
		rs.next(); //go to first record
		double dbProdPrice = rs.getDouble("prodPrice");
		
		if (actualProdPrice==dbProdPrice)
			System.out.println("Pass");
		else
			System.out.println("Fail");
		
	}

}
