package databaseConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ClassJDBC {

	public static void main(String[] args) throws SQLException {

//      https://downloads.mysql.com/archives/c-j
		//download jar  or add dependency in POM
		
		//connect db 
		Connection conn;
		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/dbmicrotech", "root", "root" );

		//declare output variable
		Statement stmt;
		stmt = conn.createStatement();

		//set output
		//ResultSet  rs = stmt.executeQuery("select * from tblemployee");
		
		ResultSet  rs1 = stmt.executeQuery("select empName from tblemployee WHERE empId=1");
		
		
		//print output
		System.out.println( "Id\tEmp name\tState"  );
		while (rs1.next()) {
			System.out.println( rs1.getString("empid") + "\t" + rs1.getString("empName") + "\t\t" + rs1.getString("empState")  );
		}
		
		//connection close
		if (!conn.isClosed()) {
			conn.close();
		}
		
		
	}

}
